--Create user list database
CREATE DATABASE userList;

--Ceate user's database 
CREATE DATABASE userName; --userName is not static

--CREATE product database
CREATE DATABASE productList; 



--Create user table
CREATE TABLE users(
    id int(11) AUTO_INCREMENT PRIMARY KEY not null,
    userName varchar(250) not null,
    eMail varchar(250) not null,
    userPassword longtext not null,
    recoveryKey varchar(20) DEFAULT '',
    keyExpire datetime DEFAULT 0
)

--Create R-shirt table
CREATE TABLE shirt{
    id int(11) AUTO_INCREMENT PRIMARY KEY not null,
    productId VARCHAR(250) not null,
    productName VARCHAR(250) not null,
    productImage VARCHAR(250) not null,
    productPrice int(11) not null,
    productSale int(11),
    inStock int(11) not null
}

--Create user cart 
CREATE TABLE cart{
    id int(11) AUTO_INCREMENT PRIMARY KEY not null,

}