<?php
session_start();
if(isset($_POST['keySubmit'])){
	include_once '../../Controller/registration/passwordReset.php';
	$givenkey = $_POST['givenkey'];
	$userMail = $_SESSION["userMail"];

	if(passwordReset::compareKey($userMail,$givenkey)){
		$_SESSION["enteredKey"] = $_POST['givenkey'];
		exit(json_encode(array("status" => true, "message" => 'correct')));
	}else{
		exit(json_encode(array("status" => false, "message" => 'incorrectKey')));
	}
}