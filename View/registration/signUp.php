<?php 
if(isset($_POST['signUp'])){
    include_once "../../Controller/registration/signController.php";
    include_once "../../Controller/registration/checkController.php";
    
    $name = $_POST['regName'];
    $mail = $_POST['regMail'];
    $password = $_POST['regPassword'];
    $confirm = $_POST['rePassword'];

    if(empty($name)||empty($password)||empty($mail)||empty($confirm)){
        exit(json_encode(array("status" => false, "message" => 'emptyField')));
    }
    else{
        if($password == $confirm){
            //if there is no user with this name,email or password
            if(checkController::newUserName($name) && checkController::newEmail($mail)){
                $hashed_password = password_hash($password, PASSWORD_DEFAULT);
                signController::addUser($name,$hashed_password,$mail);
                exit(json_encode(array("status" => true, "message" => 'userAdded')));
            }else{
                if(!checkController::newUserName($name)){
                    if(!checkController::newEmail($mail)){
                        exit(json_encode(array("status" => false, "message" => 'alreadyExists')));
                    }else{
                        exit(json_encode(array("status" => false, "message" => 'userNameExists')));
                    }
                }else{
                    exit(json_encode(array("status" => false, "message" => 'emailExists')));
                }
            }
        }
        else{
            exit(json_encode(array("status" => false, "message" => 'wrongConfirm')));
        }
    }
}
else{
    exit(json_encode(array("status" => false, "message" => 'noButtonSubmitted')));
}
