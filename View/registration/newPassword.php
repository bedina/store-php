<?php 
if(isset($_POST["passwordSubmit"])){
    $Password = $_POST["newPassword"];
    $confirm =$_POST["confirmPassword"];
    $newPassword = password_hash($Password, PASSWORD_DEFAULT);

    if($Password == $confirm){
        include_once '../../Controller/registration/passwordReset.php';
        session_start();

        $userMail = $_SESSION["userMail"];
        $givenKey = $_SESSION["enteredKey"];

        if(passwordReset::newPassword($userMail,$givenKey,$newPassword)){
            exit(json_encode(array("status" => true, "message" => 'success')));
        }
    }
}