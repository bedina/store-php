<?php

if(isset($_POST['signIn'])){
    include_once '../../Controller/registration/signController.php';

    $userLog = $_POST['logUser'];
    $password = $_POST['logPassword'];

    if(empty($userLog)||empty($password)){
        exit(json_encode(array("status" => false, "message" => 'emptyfields')));;
    }
    else{
        $log = signController::signUser($userLog,$password);
        if($log['done']){
            session_start();
            $_SESSION["userId"] = $log['userId'];
            $_SESSION["userName"] = $log['userName'];
            $_SESSION["eMail"] = $log['eMail'];

            exit(json_encode(array("status" => true, "message" => 'correct')));
        }
        else{
            exit(json_encode(array("status" => false, "message" => 'wrongLogs')));;
        }
    }
}
else{
    exit(json_encode(array("status" => false, "message" => 'noButton')));
}
