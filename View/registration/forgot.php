<?php

use PHPMailer\PHPMailer\PHPMailer;

function makeNewKey($len = 6) {
		$key = "ABCDEFGHIJKLMNOPQRTSUVWXYZJ1234567890";
		$key = str_shuffle($key);
		$key = substr($key, 0, $len);

		return $key;
}

if(isset($_POST['recover'])){
	include_once '../../Controller/registration/passwordReset.php';
	include_once "../../Controller/registration/checkController.php";

	session_start();
	
	$recoveryMail = $_POST['recoveryMail'];

	if(!checkController::newEmail($recoveryMail)){
		$newKey = makeNewKey();
		$_SESSION["userMail"] = $recoveryMail;

		if(passwordReset::insertKey($recoveryMail,$newKey)){
			$subject = "პაროლის აღდგენის კოდი";
			$body = "
	
				გამარჯობათ<br><br>
	
				თქვენი ერთჯერადი კოდი პაროლის აღდენისთვის არის <br>
	
				<h1 style='color: #dc143c;'>$newKey</h1>
				<br>
	
				გთხოვთ გაითვალისწინოთ, რომ კოდს ვადა გასდის 5 წუთში <br><br>
				წარმატებები<br><br>
	
				<a style='text-decoration:none; color: #06302d;' href='https://broody.com'>broody.com</a>
			";
				
			require_once "../../PHPMailer/PHPMailer.php";
			require_once "../../PHPMailer/SMTP.php";
			require_once "../../PHPMailer/Exception.php";
	
			$mail = new PHPMailer();
	
			//SMTP Settings
			$mail->isSMTP();
			$mail->Host = "smtp.gmail.com";
			$mail->SMTPAuth = true;
			$mail->Username = 'broody.georgia@gmail.com';
			$mail->Password = 'Broody@ertidanrvamde';
			$mail->Port = 465; //587
			$mail->SMTPSecure = "ssl"; //tls
	
			//Email Settings
			$mail->isHTML(true);
			$mail->CharSet = 'UTF-8';
			$mail->setFrom("broody.georgia@gmail.com", "broody - free online shop");
			$mail->addAddress($recoveryMail);
			$mail->Subject = $subject;
			$mail->Body = $body;
			if($mail->send()){
				exit(json_encode(array("status" => true, "message" => 'success')));
			}
		}
	}else{
		exit(json_encode(array("status" => false, "message" => 'noUser')));
	}
}
	