//email submit form
let recoverForm = $("#recover-form");
let enteredMail = $("#recovery-mail");
let errorMessege = $("#recover-mail-error");
let submitButton = $("#recover-button");

const imageLink = '<img src="../img/alert.png" alt="error">';

//email verification
function emailVerify(email) {
    let extintion = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return extintion.test(email);
}

$(document).ready(function() {
    submitButton.on('click', function() {
        if (enteredMail.val() != "" && emailVerify(enteredMail.val())) {
            enteredMail.css('border', 'none');
            errorMessege.css('display', 'none');
            $.ajax({
                type: 'POST',
                url: '../../View/registration/forgot.php',
                dataType: 'json',
                data: {
                    recover: submitButton.val(),
                    recoveryMail: enteredMail.val()
                },
                success: function(response) {
                    if (response.status) {
                        enteredMail.css('border', '1px solid #cef048');
                        errorMessege.css('display', 'none');
                        errorMessege.html("კოდი გაგზავნილია მითითებულ მეილზე");
                        errorMessege.css('display', 'block');
                        errorMessege.css('color', '#1cbbb4');
                        location.href = 'reset.php';
                    } else {
                        errorMessege.html(imageLink + "მომხმარებელი არ არსებობს");
                        errorMessege.css('display', 'block');
                        enteredMail.css('border', '1px solid crimson');
                    }
                }
            });
        } else {
            if (!emailVerify(enteredMail.val())) {
                enteredMail.css('border', '1px solid crimson');
                errorMessege.html(imageLink + "გთხოვთ შეიყვანეთ სწორი იმეილი");
            }
            if (enteredMail.val() == "") {
                enteredMail.css('border', '1px solid crimson');
                errorMessege.html(imageLink + "გთხოვთ შეიყვანეთ იმეილი");
            }
        }
    });
});