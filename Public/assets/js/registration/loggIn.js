//registrate
let userName = $("#reg-name");
let userNameError = $("#reg-name-error");
let userEmail = $("#reg-mail");
let userEmailError = $("#reg-mail-error");
let newPassword = $("#reg-password");
let newPasswordError = $("#reg-passwd-error");
let confirmPassword = $("#re-password");
let confirmPasswordError = $("#reg-conf-error");
let regSubmit = $("#sign-up-button");

//log in
let logName = $("#sing-in-log");
let logNameError = $("#log-user-error");
let logPassword = $("#sing-in-passwd");
let logPasswordError = $("#log-password-error");
let logSubmit = $("#sign-in-button");

//both
const imageLink = '<img src="../img/alert.png" alt="error">';

//email verification
function emailVerify(email) {
    let extintion = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return extintion.test(email);
}

$(document).ready(function() {
    //sign up
    regSubmit.on('click', function() {
        if (userName.val() != "" && userEmail.val() != "" && emailVerify(userEmail.val()) && newPassword.val() != "" && confirmPassword.val() != "" && newPassword.val() == confirmPassword.val()) {

            userName.css('border', '1px solid #cef048');
            userNameError.css('display', 'none');
            userEmail.css('border', '1px solid #cef048');
            userEmailError.css('display', 'none');
            newPassword.css('border', '1px solid #cef048');
            newPasswordError.css('display', 'none');
            confirmPassword.css('border', '1px solid #cef048');
            confirmPasswordError.css('display', 'none');

            $.ajax({
                type: 'POST',
                url: '../../View/registration/signUp.php',
                dataType: 'json',
                data: {
                    signUp: regSubmit.val(),
                    regName: userName.val(),
                    regMail: userEmail.val(),
                    regPassword: newPassword.val(),
                    rePassword: confirmPassword.val()
                },
                success: function(response) {
                    if (response.status) {
                        if (response.message == "userAdded") {
                            confirmPasswordError.html("მომხმარებელი დამატებულია");
                            confirmPasswordError.css('display', 'block');
                            confirmPasswordError.css('color', '#1cbbb4');
                            location.href = 'loggIn.php';
                        }

                    } else {
                        if (response.message == "alreadyExists") {
                            userNameError.html(imageLink + "სახელი უკვე გამოყენებულია");
                            userNameError.css('display', 'block');
                            userNameError.css('color', 'crimson');
                            userEmailError.html(imageLink + "იმეილი უკვე გამოყენებულია");
                            userEmailError.css('display', 'block');
                            userEmailError.css('color', 'crimson');
                        }
                        if (response.message == "userNameExists") {
                            userNameError.html(imageLink + "სახელი უკვე გამოყენებულია");
                            userNameError.css('display', 'block');
                            userNameError.css('color', 'crimson');
                        }
                        if (response.message == "emailExists") {
                            userEmailError.html(imageLink + "იმეილი უკვე გამოყენებულია");
                            userEmailError.css('display', 'block');
                            userEmailError.css('color', 'crimson');
                        }
                    }
                }

            });
        } else {
            //check if empty
            if (userName.val() == "" && userEmail.val() == "" && newPassword.val() == "" && confirmPassword.val() == "") {
                userName.css('border', '1px solid crimson');
                userEmail.css('border', '1px solid crimson');
                newPassword.css('border', '1px solid crimson');
                confirmPassword.css('border', '1px solid crimson');
                confirmPasswordError.html(imageLink + "გთხოვთ შეიყვანეთ გრაფები");
                confirmPasswordError.css('display', 'block');
                confirmPasswordError.css('color', 'crimson');
            } else {
                if (userName.val() == "") {
                    userNameError.html(imageLink + "გთხოვთ შეიყვანეთ გრაფა");
                    userNameError.css('display', 'block');
                    userNameError.css('color', 'crimson');
                } else {
                    userName.css('border', '1px solid #cef048');
                    userNameError.css('display', 'none');
                }
                if (userEmail.val() == "") {
                    userEmailError.html(imageLink + "გთხოვთ შეიყვანეთ გრაფა");
                    userEmailError.css('display', 'block');
                    userEmailError.css('color', 'crimson');
                } else {
                    //check if email is valid
                    if (emailVerify(userEmail.val())) {
                        userEmail.css('border', '1px solid #cef048');
                        userEmailError.css('display', 'none');
                    } else {
                        userEmailError.html(imageLink + "გთხოვთ სწორი იმეილი");
                        userEmailError.css('display', 'block');
                        userEmailError.css('color', 'crimson');
                    }
                }
                if (newPassword.val() == "") {
                    newPasswordError.html(imageLink + "გთხოვთ შეიყვანეთ გრაფა");
                    newPasswordError.css('display', 'block');
                    newPasswordError.css('color', 'crimson');
                } else {
                    newPassword.css('border', '1px solid #cef048');
                    newPasswordError.css('display', 'none');
                }
                if (confirmPassword.val() == "") {
                    confirmPasswordError.html(imageLink + "გთხოვთ შეიყვანეთ გრაფა");
                    confirmPasswordError.css('display', 'block');
                    confirmPasswordError.css('color', 'crimson');
                } else {
                    //check if confirmed password is same as password
                    if (confirmPassword.val() != newPassword.val()) {
                        confirmPasswordError.html(imageLink + "გამეორებული პაროლს არ ემთხვევა");
                        confirmPasswordError.css('display', 'block');
                        confirmPasswordError.css('color', 'crimson');
                    } else {
                        confirmPassword.css('border', '1px solid #cef048');
                        confirmPasswordError.css('display', 'none');
                    }
                }
            }
        }

    });

    //
    //////////////////////////////////////////////////////////////////////////
    //log in
    logSubmit.on('click', function() {
        if (logName.val() != "" && logPassword.val() != "") {
            $.ajax({
                type: 'POST',
                url: '../../View/registration/signIn.php',
                dataType: 'json',
                data: {
                    signIn: logSubmit.val(),
                    logUser: logName.val(),
                    logPassword: logPassword.val(),
                },
                success: function(response) {
                    if (response.status) {
                        logName.css('border', '1px solid #cef048');
                        logNameError.css('display', 'none');
                        logPassword.css('border', '1px solid #cef048');
                        logPasswordError.html(response.message);
                        logPasswordError.css('display', 'block');
                        logPasswordError.css('color', '#1cbbb4');
                        location.href = 'index.php';
                    } else {
                        logNameError.css('display', 'none');
                        logPasswordError.html(imageLink + "პაროლი ან იმეილი არასწორია");
                        logPasswordError.css('display', 'block');
                        logPasswordError.css('color', 'crimson');
                        logPasswordError.css('display', 'block');
                        logPasswordError.css('color', 'crimson');
                        logName.css('border', '1px solid crimson');
                        logPassword.css('border', '1px solid crimson');
                    }
                }
            });
        } else {
            if (logName.val() == "" && logPassword.val() == "") {
                logPasswordError.html(imageLink + "გთხოვთ შეიყვანეთ გრაფები");
                logPasswordError.css('display', 'block');
                logPasswordError.css('color', 'crimson');
                logName.css('border', '1px solid crimson');
                logPassword.css('border', '1px solid crimson');
            } else {
                if (logName.val() == "") {
                    logNameError.html(imageLink + "გთხოვთ შეიყვანეთ მეილი ან სახელი");
                    logNameError.css('display', 'block');
                    logNameError.css('color', 'crimson');
                    logName.css('border', '1px solid crimson');
                } else {
                    logName.css('border', '1px solid #cef048');
                    logNameError.css('display', 'none');
                }
                if (logPassword.val() == "") {
                    logPasswordError.html(imageLink + "გთხოვთ შეიყვანეთ პაროლი");
                    logPasswordError.css('display', 'block');
                    logPasswordError.css('color', 'crimson');
                    logPassword.css('border', '1px solid crimson');
                } else {
                    logPassword.css('border', '1px solid #cef048');
                    logPasswordError.css('display', 'none');
                }
            }
        }
    });
});