let passwordForm = $("#password-form");
let newPassword = $("#new-password");
let confirmPassword = $("#confirm-password");
let passwordErrorMessege = $("#password-error");
let confirmErrorMessege = $("#confirm-error");
let passwordSubmitButton = $("#password-button");

const imageLink = '<img src="../img/alert.png" alt="error">';

$(document).ready(function() {
    passwordSubmitButton.on('click', function() {
        if (newPassword.val() != "" && confirmPassword.val() != "" && newPassword.val() == confirmPassword.val()) {
            $.ajax({
                type: 'POST',
                url: '../../View/registration/newPassword.php',
                dataType: 'json',
                data: {
                    passwordSubmit: passwordSubmitButton.val(),
                    newPassword: newPassword.val(),
                    confirmPassword: confirmPassword.val()
                },
                success: function(response) {
                    if (response.status) {
                        newPassword.css('border', '1px solid #cef048');
                        passwordErrorMessege.css('display', 'none');
                        confirmPassword.css('border', '1px solid #cef048');
                        confirmErrorMessege.css('display', 'none');
                        confirmErrorMessege.html(response.message);
                        confirmErrorMessege.css('display', 'block');
                        confirmErrorMessege.css('color', '#1cbbb4');
                        location.href = 'loggIn.php';
                    }
                }
            });
        } else {
            if (newPassword.val() == "" && confirmPassword.val() == "") {
                confirmErrorMessege.html(imageLink + "გთხოვთ შეიყვანეთ ახალი პაროლი");
                confirmErrorMessege.css('display', 'block');
                confirmErrorMessege.css('color', 'crimson');
                newPassword.css('border', '1px solid crimson');
                confirmPassword.css('border', '1px solid crimson');
            } else {
                if (newPassword.val() == "") {
                    passwordErrorMessege.html(imageLink + "გთხოვთ შეიყვანეთ პაროლი");
                    passwordErrorMessege.css('display', 'block');
                    passwordErrorMessege.css('color', 'crimson');
                    newPassword.css('border', '1px solid crimson');
                } else {
                    newPassword.css('border', '1px solid #cef048');
                    passwordErrorMessege.css('display', 'none')
                }
                if (newPassword.val() != confirmPassword.val()) {
                    if (confirmPassword.val() == "") {
                        confirmErrorMessege.html(imageLink + "გთხოვთ გაიმეორეთ პაროლი");
                        confirmErrorMessege.css('display', 'block');
                        confirmErrorMessege.css('color', 'crimson');
                        confirmPassword.css('border', '1px solid crimson');
                    } else {
                        confirmErrorMessege.html(imageLink + "გამეორებული პაროლი არ ემთხვევა");
                        confirmErrorMessege.css('display', 'block');
                        confirmErrorMessege.css('color', 'crimson');
                        confirmPassword.css('border', '1px solid crimson');
                    }
                }
            }
        }
    });
});