let keyForm = $("#reset-form");
let recoveryKey = $("#recovery-key");
let keyErrorMessege = $("#key-error");
let keySubmitButton = $("#key-button");

const imageLink = '<img src="../img/alert.png" alt="error">';

$(document).ready(function() {
    keySubmitButton.on('click', function() {
        if (recoveryKey.val() != "") {
            $.ajax({
                type: 'POST',
                url: '../../View/registration/reset.php',
                dataType: 'json',
                data: {
                    keySubmit: keySubmitButton.val(),
                    givenkey: recoveryKey.val()
                },
                success: function(response) {
                    if (response.status) {
                        recoveryKey.css('border', '1px solid #cef048');
                        keyErrorMessege.html("კოდი სწორია");
                        keyErrorMessege.css('display', 'block');
                        keyErrorMessege.css('color', '#1cbbb4');
                        location.href = 'newPassword.php';
                    } else {
                        keyErrorMessege.html(imageLink + "გთხოვთ ჩაწერეთ სწორი კოდი");
                        keyErrorMessege.css('display', 'block');
                        keyErrorMessege.css('color', 'crimson');
                        recoveryKey.css('border', '1px solid crimson');
                    }
                }
            });
        } else {
            keyErrorMessege.html(imageLink + "გთხოვთ შეიყვანეთ კოდი");
            keyErrorMessege.css('display', 'block');
            keyErrorMessege.css('color', 'crimson');
            recoveryKey.css('border', '1px solid crimson');
        }
    });
});