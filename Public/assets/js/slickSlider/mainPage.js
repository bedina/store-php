$(document).ready(function() {
    $('#img-slider').slick({
        asNavFor: $('#text-slider'),
        dots: false,
        arrows: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 10000,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: true
    });
    $('#text-slider').slick({
        asNavFor: $('#img-slider'),
        dots: true,
        arrows: true,
        infinite: true,
        autoplay: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: true,
        prevArrow: '<span class="slickArrow slickArrow-prev"><img src="../img/prev.png" alt=""></span>',
        nextArrow: '<span class="slickArrow slickArrow-next"><img src="../img/next.png" alt=""></span>'
    });
});