<!DOCTYPE html>
<html>

<head>
    <title> recover password </title>
    <link rel="stylesheet" href="../assets/css/forgot.css">
    <link rel="stylesheet" href="../assets/css/reset.css">
    <link rel="stylesheet" href="../assets/css/newPassword.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <!--<script src="../assets/js/registration/forgot.js"></script>-->
</head>

<body>
    <header>
        <?php
            require_once 'global/navBar.php';
        ?>
    </header>
    <main>
    <div class="recover-container">
            <form id="recover-form">
                <div id="text-header">
                    <h1>პაროლის აღდგენა</h1>
                </div>
                <div id="email-input">
                    <p class="help">გთხოვთ შეიყვანოთ იმეილის მისამართი, რომელიც ასოცირებულია პროფილთან</p>
                    <input id="recovery-mail" type="text" name="recoveryMail" placeholder="შეიყვანეთ მეილის მისამართი" />
                    <p class="error" id="recover-mail-error"></p>
                </div>
                <div id="reset-submit">
                    <input id="recover-button" type="button" name="recover" value="შეყვანა"/>
                </div>
            </form>
    </div>
    </main>
    <script type="text/javascript" src="../assets/js/registration/forgot.js"></script>
</body>

</html>