<!DOCTYPE html>
<html>

<head>
    <title> Home Page </title>
    <link rel="stylesheet" href="../assets/css/home.css">
    <link rel="stylesheet" href="../assets/css/slick.css">
    <link rel="stylesheet" href="../assets/css/slick-theme.css">
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="../assets/js/slickSlider/mainPage.js"></script>
    <script src="../assets/js/slickSlider/slick.js"></script>
    <script src="../assets/js/slickSlider/slick.min.js"></script>
</head>
<header>
        <?php
            require_once 'global/navBar.php';
        ?>
</header>
<body>
    <div class="wrapper">
        <section id="search-section">
            <h1 id="discover">აღმოაჩინე</h1>
            <form class="search">
                <input id="search-bar" type="text" name="searchThis" placeholder="დაიწყე ძიება">
                <button id="search-button" type="submit" name="search"><img src="../img/search.png" alt=""></button>
            </form>
        </section>
        <section id="slick-slider">
            <div id="slider">
                <div id="img-slider">
                    <img src="../img/shirt1.png" alt="">
                    <img src="../img/shirt2.png" alt="">
                    <img src="../img/shirt3.png" alt="">
                </div>
                <div id="about">
                    <div id="text-slider">
                        <div class="about">
                            <h1>shirt 1</h1>
                            <p></p>
                            <a class="learn-more" href="store.php"><h5>გაიგე მეტი<img src="../img/link.png" alt=""></h5></a>
                        </div>
                        <div class="about">
                            <h1>shirt 2</h1>
                            <a class="learn-more" href="store.php"><h5>გაიგე მეტი<img src="../img/link.png" alt=""></h5></a>
                        </div>
                        <div class="about">
                            <h1>shirt 3</h1>
                            <a class="learn-more" href="store.php"><h5>გაიგე მეტი<img src="../img/link.png" alt=""></h5></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="product-sales product-demo">
            <div class="section-title">
                <h1 class="title">ფასდაკლება</h1>
                <a class="show-more" href="store.php">ნახეთ მეტი</a>
            </div>
            <div class="product-list">
                <div class="product">
                    <img class="product-image" src="../img/hungry.png" alt="">
                    <h5 class="product-name">hungry shirt</h5>
                    <p class="product-description">this is some random hungry shirt</p>
                    <p class="product-price">39.99$</p>
                </div>
                <div class="product">
                    <img class="product-image" src="../img/hungry.png" alt="">
                    <h5 class="product-name">hungry shirt</h5>
                    <p class="product-description">this is some random hungry shirt</p>
                    <p class="product-price">39.99$</p>
                </div>
                <div class="product">
                    <img class="product-image" src="../img/hungry.png" alt="">
                    <h5 class="product-name">hungry shirt</h5>
                    <p class="product-description">this is some random hungry shirt</p>
                    <p class="product-price">39.99$</p>
                </div>
                <div class="product">
                    <img class="product-image" src="../img/hungry.png" alt="">
                    <h5 class="product-name">hungry shirt</h5>
                    <p class="product-description">this is some random hungry shirt</p>
                    <p class="product-price">39.99$</p>
                </div>
            </div>
        </section>
        <section class="about-survey info-demo">
            <div class="section-text">
                <h1 class="section-title">
                    გაადროვეთ ქულები
                </h1>
                <p class="section-description">
                    გაიარეთ გამოკითხვა, ითამაშეთ, გაერთეთ, დაგროვილი ქულები და ისინი გადაცვალეთ სასურველ პროდუქტში
                </p>
                <button class="learn-more">
                 გაიგეთ მეტი
                </button>
            </div>
            <div class="section-image pig-safe">
                <img src="../img/save.png" alt="" class="save-coins">
            </div>
        </section> 
        <section class="hot-product product-demo">
            <div class="section-title">
                <h1 class="title">პოპულარული პროდუქტი</h1>
                <a class="show-more" href="store.php">ნახეთ მეტი</a>
            </div>
            <div class="product-list">
                <div class="product">
                    <img class="product-image" src="../img/mom.png" alt="">
                    <h5 class="product-name">mom shirt</h5>
                    <p class="product-description">this is some random mom shirt</p>
                    <p class="product-price">39.99$</p>
                </div>
                <div class="product">
                    <img class="product-image" src="../img/mom.png" alt="">
                    <h5 class="product-name">mom shirt</h5>
                    <p class="product-description">this is some random mom shirt</p>
                    <p class="product-price">39.99$</p>
                </div>
                <div class="product">
                    <img class="product-image" src="../img/mom.png" alt="">
                    <h5 class="product-name">mom shirt</h5>
                    <p class="product-description">this is some random mom shirt</p>
                    <p class="product-price">39.99$</p>
                </div>
                <div class="product">
                    <img class="product-image" src="../img/mom.png" alt="">
                    <h5 class="product-name">mom shirt</h5>
                    <p class="product-description">this is some random mom shirt</p>
                    <p class="product-price">39.99$</p>
                </div>
            </div>
        </section>
        <section class="future-update info-demo">
            <div class="section-text">
                <h1 class="section-title">
                    მალე დაემარება
                </h1>
                <p class="section-description">
                    დააგტოვეთ ქულები და გადაცვალეთ ისინი სასურველ სასაჩუქრე ბარათში.
                </p>
                <button class="learn-more">
                 გაიგეთ მეტი
                </button>
            </div>
            <div class="section-image gift-cards">
                <img src="../img/gifts.png" alt="" class="card-images">
            </div>
        </section>
        <section class="new-product product-demo">
            <div class="section-title">
                <h1 class="title">ახალი პროდუქცია</h1>
                <a class="show-more" href="store.php">ნახეთ მეტი</a>
            </div>
            <div class="product-list">
                <div class="product">
                    <img class="product-image" src="../img/broody.png" alt="">
                    <h5 class="product-name">broody shirt</h5>
                    <p class="product-description">this is some random broody shirt</p>
                    <p class="product-price">39.99$</p>
                </div>
                <div class="product">
                    <img class="product-image" src="../img/broody.png" alt="">
                    <h5 class="product-name">broody shirt</h5>
                    <p class="product-description">this is some random broody shirt</p>
                    <p class="product-price">39.99$</p>
                </div>
                <div class="product">
                    <img class="product-image" src="../img/broody.png" alt="">
                    <h5 class="product-name">broody shirt</h5>
                    <p class="product-description">this is some random broody shirt</p>
                    <p class="product-price">39.99$</p>
                </div>
                <div class="product">
                    <img class="product-image" src="../img/broody.png" alt="">
                    <h5 class="product-name">broody shirt</h5>
                    <p class="product-description">this is some random broody shirt</p>
                    <p class="product-price">39.99$</p>
                </div>
            </div>
        </section>
    </div>
</body>
<footer>
        <?php
            require_once 'global/copyrightFooter.php';
        ?>
</footer>

</html>