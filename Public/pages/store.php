<!DOCTYPE html>
<html>

<head>
    <title> Store </title>
    <link rel="stylesheet" href="../assets/css/store.css">
    
</head>
<header>
    <?php
        require_once 'global/navBar.php';
    ?>
</header>
<body>
    <main>
        <div class="wrapper">
           <section id="search-section">
                <form class="search">
                    <input id="search-bar" type="text" name="searchThis" placeholder="დაიწყე ძიება">
                    <button id="search-button" type="submit" name="search"><img src="../img/search.png" alt=""></button>
                </form>
            </section>
            <section id="all-product">
                <div class="sort">
                    <select id="sort-product" name="sorter">
                        <option value="price-down" name="price-down">ფასი კლებადი</option>
                        <option value="price-up" name="price-up">ფასი ზრდადი</option>
                        <option value="new" name="new">უახლესი</option>
                        <option value="popular" name="popular">პოპულალური</option>
                    </select>
                </div>
                <div class="product-show">
                    <div class="product-nav">
                        <ul>
                            <li></li>
                        </ul>
                    </div>
                    <div class="all-product-list">

                    </div>
                </div>
            </section> 
        </div>
    </main>
</body>
<footer>
    <?php
        //require_once 'global/copyrightFooter.php';
    ?>
</footer>
</html>