<?php 
    session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title> reset password </title>
    <link rel="stylesheet" href="../assets/css/reset.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>

<body>
    <header>
        <?php
            require_once 'global/navBar.php';
        ?>
    </header>
    <main>
    <div class="reset-container">
            <form id="reset-form" method="POST">
                <div id="text-header">
                    <h1>პაროლის აღდგენა</h1>
                </div>
                <div id="key-input">
                    <p class="help">შეიყვანეთ ერთჯერადი ექვსნიშნა კოდი, რომელიც მიიღეთ იმელზე <?php echo $_SESSION["userMail"];?></p>
                    <input id="recovery-key" type="text" name="givenkey" placeholder="შეიყვანეთ კოდი" />
                    <p class="error" id="key-error"></p>
                </div>
                <div id="key-submit">
                    <input id="key-button" type="button" name="keySubmit" value="შეყვანა"/>
                </div>
            </form>
    </div>
    </main>
    <script src="../assets/js/registration/reset.js"></script>
</body>

</html>