<?php
    session_start();
    if(!isset($_SESSION['userId'])){
        (header("Location: loggIn.php"));
    }
?>
<!DOCTYPE html>
<html>

<head>
    <title> My Account  </title>
    <link rel="stylesheet" href="../assets/css/home.css">
    <script src="../assets/js/slickSlider/mainPage.js"></script>
    <script src="../assets/js/slickSlider/slick.js"></script>
    <script src="../assets/js/slickSlider/slick.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    
</head>

<body>
    <header>
        <?php
            require_once 'global/navBar.php';
        ?>
    </header>
    <main>
       
    </main>
    <footer>
        <?php
               // require_once 'global/copyrightFooter.php';
        ?>
    </footer>
</body>

</html>