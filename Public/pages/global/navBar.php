<?php
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
?>
<nav class="main-nav">
    <div class="logo_div">
        <a href="index.php"><img src="../img/logoWithGlasses.png" alt="Website" class="logo"></a>
    </div>
    <div class="navbar">
        <ul>
            <li><a href="index.php">სახლი</a></li>
            <li><a href="store.php">მაღაზია</a></li>
            <li><a href="survey.php">ვაშლები</a></li>
            <li><a href="about.php">მომსახრება</a></li>
            <li><a href="support.php">დახმარება</a></li>
        </ul>
    </div>
    <div class="user">
        <div class="cart">
            <a href="cart.php"><img src="../img/shoppingCart.png" alt="Cart"></a>
        </div>
        <div class="avatar">
            <img src="../img/logAvatar.png" alt="Avatar">
            <div class="avatar-menu">
            <?php 
                if(isset($_SESSION['userId'])){
                    echo '<ul>
                            <li><p id="user-name">'.$_SESSION['userName'].'</p></li>
                            <li><a href="account.php">ჩემი პროფილი</a></li>
                            <li><form action="../../View/registration/logOut.php" method="POST"><button type="submit" id="log-out" name="signOut" value="გამოსვლა"/>გამოსვლა</button></form></li>
                         </ul>  ';
                }
                else{
                    echo '<ul>
                            <li><a href="loggIn.php">ავტორიზაცია</a></li>
                            <li><a href="account.php">ჩემი პროფილი</a></li>
                         </ul>  ';
                }
            ?>
                  
            </div>
        </div>
    </div>
</nav>