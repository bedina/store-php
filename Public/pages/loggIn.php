<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Registration</title>
    <link rel="stylesheet" href="../assets/css/registration.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="../assets/js/registration/slider.js"></script>
</head>

<body>
    <header>
        <?php
            require_once 'global/navBar.php'
        ?>
    </header>
    
    <main>
        <div class="container " id="container">
            <div class="form-container sign-up-container">
                <form>
                    <h1>რეგისტრაცია</h1>
                    <input type="text" id="reg-name" placeholder="შეიყვანეთ მომხმარებლის სახელი" />
                    <p class="error" id="reg-name-error"></p>
                    <input type="text" id="reg-mail" placeholder="შეიყვანეთ იმეილის მისამართი" />
                    <p class="error" id="reg-mail-error"></p>
                    <input type="password" id="reg-password" placeholder="შეიყვანეთ პაროლი" />
                    <p class="error" id="reg-passwd-error"></p>
                    <input type="password" id="re-password" placeholder="გაიმეორეთ პაროლი" />
                    <p class="error" id="reg-conf-error"></p>
                    <button id="sign-up-button" type="button" name="signUp">დამატება</button>
                </form>
            </div>
            <div class="form-container sign-in-container">
                <form>
                    <h1>ავტორიზაცია</h1>
                    <input id="sing-in-log" type="text" name="logUser" placeholder="შეიყვანეთ სახელი ან იმელი" />
                    <p class="error" id="log-user-error"></p>
                    <input id="sing-in-passwd" type="password" name="logPassword" placeholder="შეიყვანეთ პაროლი" />
                    <p class="error" id="log-password-error"></p>
                    <a href="forgot.php">დაგავიწყდათ პაროლი?</a>
                    <button id="sign-in-button" class="btn-signin" type="button" name="signIn">შესვლა</button>
                </form>
            </div>
            <div class="overlay-container">
                <div class="overlay">
                    <div class="overlay-panel overlay-left">
                        <h1>გაქვთ პროფილი?</h1>
                        <p>იმისათვის რომ შეხვიდეთ თქვენს პროფილზე, ავტორიზაცია გაიარეთ აქ</p>
                        <button class="ghost" id="signIn" onclick='signIn()'>შესვლა</button>
                    </div>
                    <div class="overlay-panel overlay-right">
                        <h1>გააკეთეთ ახალი პროფილი</h1>
                        <p>იმისათვის რომ შემოუერთდეთ ჩვენს პლატფორმას დარეგისტრირდით აქ</p>
                        <button class="ghost" id="signUp" onclick='signUp()'>დამატება</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script src="../assets/js/registration/loggIn.js"></script>
</body>
</html>