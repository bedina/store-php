<?php 
    session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title> reset password </title>
    <link rel="stylesheet" href="../assets/css/newPassword.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>

<body>
    <header>
        <?php
            require_once 'global/navBar.php';
        ?>
    </header>
    <main>
    <div class="password-container">
            <form id="password-form" method="POST">
                <div id="text-header">
                    <h1>პაროლის აღდგენა</h1>
                </div>
                <div id="password-input">
                    <p class="help">შეიყვანეთ ახალი პაროლი</p>
                    <input id="new-password" type="password" name="newPassword" placeholder="შეიყვანეთ პაროლი" />
                    <p class="error" id="password-error"></p>
                    <input id="confirm-password" type="password" name="confirmPassword" placeholder="გაიმეორეთ პაროლი" />
                    <p class="error" id="confirm-error"></p>
                </div>
                <div id="password-submit">
                    <input id="password-button" type="button" name="passwordSubmit" value="შეყვანა"/>
                </div>
            </form>
    </div>
    </main>
    <script src="../assets/js/registration/newPassword.js"></script>
</body>

</html>