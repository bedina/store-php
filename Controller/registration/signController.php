<?php

include_once "../../Model/userRegistration.php";
include_once "../../DataBase/Dbh.php";

class signController{
    public static function signUser($userLog,$userPassword){
        $conn = Dbh::connect("userList");
        $sql = "SELECT * FROM users WHERE (userName = :userName or eMail = :Email)";
        $stmt = $conn->prepare($sql);
        $userInfo = array('userName'=>$userLog,'Email'=>$userLog);
        $stmt->execute($userInfo);
        
        $rowNum = $stmt->rowCount();

        if($row = $stmt->fetch()){
            if(password_verify ($userPassword,$row['userPassword'])){
                return array("done"=>true,"userId"=>$row['id'],"userName"=>$row['userName'],"eMail"=>$row['eMail']);
            } else {
                return array("done"=>false);
            }
        }
        else{
        }
    }

    public static function addUser($userName,$userPassword,$userMail){
        $user = new useruserList($userName,$userPassword,$userMail);
        $conn = Dbh::connect("userList");
        $sql = "INSERT INTO users(userName,eMail,userPassword) VALUES(:userName,:Email,:Password)";
        $stmt = $conn->prepare($sql);
        $userInfo = array('userName'=>$user->getName(),'Email'=>$user->getMail(),'Password'=>$user->getPassword());
        $stmt->execute($userInfo);
    }
}