<?php

include_once "../../DataBase/Dbh.php";

class passwordReset{
    public static function insertKey($recoveryMail,$newKey){
        $conn = Dbh::connect("registration");
        $sql = "SELECT * FROM users WHERE eMail = :Email";
        $stmt = $conn->prepare($sql);
        $stmt->execute(array('Email'=>$recoveryMail));
        if($row = $stmt->fetch()){
            $updateSQL = "UPDATE users SET recoveryKey = :newKey, keyExpire = DATE_ADD(NOW(),INTERVAL 5 MINUTE) WHERE eMail = :Email";
            $stmt = $conn->prepare($updateSQL);
            $stmt->execute(array('newKey'=>$newKey,'Email'=>$recoveryMail));
            return true;
        }
    }

    public static function compareKey($userMail,$givenKey){
        $conn = Dbh::connect("registration");
        $sql = "SELECT * FROM users WHERE eMail = :userMail and recoveryKey = :givenKey and keyExpire != '' and keyExpire > NOW()";
        $stmt = $conn->prepare($sql);
        $compare = array('userMail'=>$userMail,'givenKey'=>$givenKey);
        $stmt->execute($compare);
        if($row = $stmt->fetch()){
            return true;
        }
    }

    public static function newPassword($userMail,$givenKey,$newPassword){
        $conn = Dbh::connect("registration");
        $sql = "SELECT * FROM users WHERE eMail = :userMail and recoveryKey = :givenKey and keyExpire <> '' and keyExpire < NOW()";
        $stmt = $conn->prepare($sql);
        $compare = array('userMail'=>$userMail,'givenKey'=>$givenKey);
        if($stmt->execute($compare)){
            $update = array('newPassword'=>$newPassword,'userMail'=>$userMail,'givenKey'=>$givenKey);
            $updateSQL = "UPDATE users SET userPassword = :newPassword WHERE eMail = :userMail and recoveryKey = :givenKey";
            $stmt = $conn->prepare($updateSQL);
            if($stmt->execute($update)){
                return true;
            }
            else{
                return false;
            }
        }
    }
}