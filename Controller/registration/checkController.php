<?php

include_once "../../DataBase/Dbh.php";

class checkController{
    //is this new password or already exists
    public static function newPassword($password){
        $conn = Dbh::connect("userList");
        $sql = "SELECT userPassword FROM users";
        $stmt = $conn->prepare($sql);
        $stmt->execute(array("userPassword"=>$password));
        while($row = $stmt->fetch()){
            if(password_verify($password,$row['userPassword'])){
                return false;
            }else{
                return true;
            }
        }
    }   

    //is this new userName or already exists
    public static function newUserName($userName){
        $conn = Dbh::connect("userList");
        $sql = "SELECT * FROM users WHERE userName = :userName";
        $stmt = $conn->prepare($sql);
        $stmt->execute(array("userName"=>$userName));
        if($row = $stmt->fetch()){
            return false;
        }
        else{
            return true;
        }
    }

    //is this new eMail or already exists
    public static function newEmail($email){
        $conn = Dbh::connect("userList");
        $sql = "SELECT * FROM users WHERE eMail = :eMail";
        $stmt = $conn->prepare($sql);
        $stmt->execute(array("eMail"=>$email));
        if($row = $stmt->fetch()){
            return false;
        }
        else{
            return true;
        }
    }
}